package com.thiyagesh.admin.child.utlis;

import android.content.Context;
import android.content.SharedPreferences;

import android.util.Log;


import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public final class FireStoreCloud {

    public static void cloud_Ride_Key(String Ride_Key, FirebaseFirestore db,String lat, String lon) {
        Map<String, Object> user = new HashMap<>();
        user.put("BUS_STATUS", Ride_Key);
        user.put("LOCATION", lat + "," + lon);
        db.collection(
                "BUS_STATUS")
                .document(Ride_Key)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure( Exception e) {
                        Log.e("Firebase", "onFailure: fail ");

                    }
                })

                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( Task<Void> task) {
                        Log.e("Firebase", "onComplete: success");
                    }
                });
    }

    public static void  updateLatLong(String Ride_Key,String lat, String lon) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> user = new HashMap<>();
        user.put("BUS_STATUS", Ride_Key);
        user.put("LOCATION", lat + "," + lon);
        db.collection(
                "BUS_STATUS")
                .document(Ride_Key)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure( Exception e) {
                        Log.e("Firebase", "onFailure: fail ");

                    }
                })

                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( Task<Void> task) {
                        Log.e("Firebase", "onComplete: success");
                    }
                });

    }

}
