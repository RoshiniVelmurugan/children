package com.thiyagesh.admin.child

import android.Manifest
import android.content.*
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.preference.PreferenceManager
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.thiyagesh.admin.child.utlis.FireStoreCloud
import com.thiyagesh.admin.child.utlis.LoginActivity
import java.util.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, OnSharedPreferenceChangeListener,LocationInterface {



    private val TAG: String = MapsActivity::class.java.getSimpleName()
    private var mMap: GoogleMap? = null
    // Used in checking for runtime permissions.
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    private val db = FirebaseFirestore.getInstance()

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private var myReceiver: MyReceiver? = null
    private var currentLocationMarker: Marker? = null
    val DEFAULT_ZOOM = 17
    //firebase auth object
    private var firebaseAuth: FirebaseAuth? = null
    // A reference to the service used to get location updates.
    private var mService: LocationUpdatesService? = null

    // Tracks the bound state of the service.
    private var mBound = false

    // UI elements.
    private var mRequestLocationUpdatesButton: Button? = null
    private var mRemoveLocationUpdatesButton: Button? = null

    // Monitors the state of the connection to the service.
    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder: LocationUpdatesService.LocalBinder =
                service as LocationUpdatesService.LocalBinder
            mService = binder.getService()
            mBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myReceiver = MyReceiver()
        val logoutButton = findViewById<Button>(R.id.logout_btn)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        // Check that the user hasn't revoked permissions by going to Settings.
        if (Utils.requestingLocationUpdates(this)) {
            if (!checkPermissions()) {
                requestPermissions()
            }
        }
        setLastLocationToFirestore("ef", "sffs")
        New_RIde_key()
        val obj = MapsActivity()
        // val handler = Handler()
        //handler.postDelayed({  obj.intiMyLocation(this) }, 1000)

        val mainHandler = Handler(Looper.getMainLooper())

        mainHandler.post(object : Runnable {
            override fun run() {
                intiMyLocation()
                mainHandler.postDelayed(this, 60000)
            }
        })
        Utils.SetDeviceid(this,"STUDENT")

//        logoutButton.setOnClickListener {
//           Intent (this, Tour::class.java)
//
//        }
//        logoutButton!!.setOnClickListener {
//            logout()
//        }
    }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)

        mRequestLocationUpdatesButton =
            findViewById<View>(R.id.request_location_updates_button) as Button
        mRemoveLocationUpdatesButton =
            findViewById<View>(R.id.remove_location_updates_button) as Button

        mRequestLocationUpdatesButton!!.setOnClickListener(View.OnClickListener {
            if (!checkPermissions()) {
                requestPermissions()
            } else {
                mService!!.requestLocationUpdates()
            }
        })

        mRemoveLocationUpdatesButton!!.setOnClickListener(View.OnClickListener { mService!!.removeLocationUpdates() })


        // Restore the state of the buttons when the activity (re)launches.
        //setButtonsState(Utils.requestingLocationUpdates(this))
        // Bind to the service. If the service is in foreground mode, this signals to the service
// that since this activity is in the foreground, the service can exit foreground mode.
        bindService(
            Intent(this, LocationUpdatesService::class.java), mServiceConnection,
            Context.BIND_AUTO_CREATE
        )
    }


    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            myReceiver!!,
            IntentFilter(LocationUpdatesService.ACTION_BROADCAST)
        )


    }


    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver!!)
        super.onPause()
    }


    private fun logout() {
        firebaseAuth!!.signOut()
        //closing activity
        finish()
        val b = Intent (this, LoginActivity::class.java)
        startActivity(b)
    }

    override fun onStop() {
        if (mBound) { // Unbind from the service. This signals to the service that this activity is no longer
// in the foreground, and the service can respond by promoting itself to a foreground
// service.
            unbindService(mServiceConnection)
            mBound = false
        }
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)
        super.onStop()
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale =
            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        // Provide an additional rationale to the user. This would happen if the user denied the
// request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(
                TAG,
                "Displaying permission rationale to provide additional context."
            )
            Snackbar.make(
                findViewById<View>(R.id.map),
                R.string.permission_rationale,
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(R.string.ok, View.OnClickListener {
                    // Request permission
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_PERMISSIONS_REQUEST_CODE
                    )
                })
                .show()
        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
// sets the permission in a given state or the user denied the permission
// previously and checked "Never ask again".
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>,
        grantResults: IntArray
    ) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) { // If user interaction was interrupted, the permission request is cancelled and you
// receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) { // Permission was granted.
                this.mService!!.requestLocationUpdates()
            } else { // Permission denied.
                setButtonsState(false)
                Snackbar.make(
                    findViewById<View>(R.id.map),
                    R.string.permission_denied_explanation,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.settings, View.OnClickListener {
                        // Build intent that displays the App settings screen.
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts(
                            "package",
                            BuildConfig.APPLICATION_ID, null
                        )
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    })
                    .show()
            }
        }
    }

    /**
     * Receiver for broadcasts sent by [LocationUpdatesService].
     */
    private class MyReceiver() : BroadcastReceiver() {
        private val db = FirebaseFirestore.getInstance()
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {
            val location =
                intent.getParcelableExtra<Location>(LocationUpdatesService.EXTRA_LOCATION)
            if (location != null) {
                Utils.Setlocation(context, location)



                Toast.makeText(
                    context
                    , Utils.getLocationText(location),
                    Toast.LENGTH_SHORT
                ).show()
                val docRef = db.collection("STUDENT_STATUS").document(Utils.getDeviceid(context))

                FireStoreCloud.cloud_Ride_Key(Utils.getDeviceid(context), db,  Utils.requesting_latitude(context), Utils.requesting_longitude(context))
                docRef.get().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val document = task.result!!
                        if (document.exists() && !document.metadata.hasPendingWrites()) {
                            FireStoreCloud.cloud_Ride_Key(Utils.getDeviceid(context), db,  Utils.requesting_latitude(context), Utils.requesting_longitude(context))
                        } else {
                            Log.d("cloud_Ride_Key_Status", "No such document")
                            // Toaster.INSTANCE.show(getMvpView().getContext(),"Check Your Internet");
                            // FireStoreCloud.cloud_Ride_Key_Update(getDataManager().getRide_key_Local());
                            FireStoreCloud.cloud_Ride_Key(Utils.getDeviceid(context), db,  Utils.requesting_latitude(context), Utils.requesting_longitude(context))

                        }
                    } else {
                        FireStoreCloud.cloud_Ride_Key(Utils.getDeviceid(context), db,  Utils.requesting_latitude(context), Utils.requesting_longitude(context))
                        Log.d("cloud_Ride_Key_Status", "get failed with ", task.exception)
                    }
                }
            }
        }

    }

    override fun onSharedPreferenceChanged(
        sharedPreferences: SharedPreferences,
        s: String
    ) { // Update the buttons state depending on whether location updates are being requested.
        if (s == Utils.KEY_REQUESTING_LOCATION_UPDATES) {
            setButtonsState(
                sharedPreferences.getBoolean(
                    Utils.KEY_REQUESTING_LOCATION_UPDATES,
                    false
                )
            )
        }
    }

    private fun setButtonsState(requestingLocationUpdates: Boolean) {
        if (requestingLocationUpdates) {
            mRequestLocationUpdatesButton!!.setEnabled(false)
            mRemoveLocationUpdatesButton!!.setEnabled(true)
        } else {
            mRequestLocationUpdatesButton!!.setEnabled(true)
            mRemoveLocationUpdatesButton!!.setEnabled(false)
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {

            mMap = googleMap

            val sydney = LatLng(Utils.requesting_latitude_long(this).toDouble(), Utils.requesting_longitude_long(this).toDouble())
            mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))
            FireStoreCloud.updateLatLong("STUDENT",
                Utils.requesting_latitude(this).toString(), Utils.requesting_longitude(this).toString()
            )


        }


    }

    /*public fun intiMyLocation() {
        val currentPosition = LatLng(Utils.requesting_latitude(this),Utils.requesting_longitude(this))
        if (currentLocationMarker == null && Utils.requesting_latitude(this) !== 0.0 && Utils.requesting_longitude(this) !== 0.0) {

            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(currentPosition, DEFAULT_ZOOM.toFloat()),
                3000,
                null
            )

        } else {
        }
    }*/

    fun intiMyLocation() {

        val currentPosition = LatLng(Utils.requesting_latitude(this).toDouble(),Utils.requesting_longitude(this).toDouble())
        if (currentLocationMarker == null && Utils.requesting_latitude(this).toDouble() !== 0.0 && Utils.requesting_longitude(this).toDouble() !== 0.0) {

            mMap!!.addMarker(MarkerOptions().position(currentPosition).title("Marker in Sydney"))
            mMap?.animateCamera(
                CameraUpdateFactory.newLatLngZoom(currentPosition, DEFAULT_ZOOM.toFloat()),
                3000,
                null
            )

        } else {
        }
    }

    private fun setLastLocationToFirestore(lat: String, lon: String) {
        /*val docRef = db.collection("BUS_STATUS").document("BUS")
        docRef.get().addOnCompleteListener(OnCompleteListener<DocumentSnapshot> { task ->
            if (task.isSuccessful) {
                val document = task.result!!
                if (document.exists() && !document.metadata.hasPendingWrites()) {

                } else {

                }
            } else {
                FireStoreCloud.cloud_Ride_Key("BUS", db)
                Log.d("cloud_Ride_Key_Status", "get failed with ", task.exception)
            }
        })*/

        val db = FirebaseFirestore.getInstance()
        val user = HashMap<String, Any>()
        user["LOCATION"] = lat + "," + lon
        db.collection("STUDENT_STATUS")
            .document(("STUDENT"))
            .update(user)
            .addOnSuccessListener { aVoid ->

            }
            .addOnFailureListener { }

    }

    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun New_RIde_key() {
        val docRef = db.collection("STUDENT_STATUS").document("STUDENT")
        Utils.SetDeviceid(this,"STUDENT")
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result!!
                if (document.exists() && !document.metadata.hasPendingWrites()) {
                    FireStoreCloud.cloud_Ride_Key("STUDENT", db,  Utils.requesting_latitude(this), Utils.requesting_longitude(this))
                } else {
                    Log.d("cloud_Ride_Key_Status", "No such document")
                    // Toaster.INSTANCE.show(getMvpView().getContext(),"Check Your Internet");
                    // FireStoreCloud.cloud_Ride_Key_Update(getDataManager().getRide_key_Local());
                    FireStoreCloud.cloud_Ride_Key("STUDENT", db,
                        Utils.requesting_latitude(this), Utils.requesting_longitude(this))
                }
            } else {
                FireStoreCloud.cloud_Ride_Key("STUDENT", db,  Utils.requesting_latitude(this), Utils.requesting_longitude(this))
                Log.d("cloud_Ride_Key_Status", "get failed with ", task.exception)
            }
        }

    }
}
