package com.thiyagesh.admin.child

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.viewpager.widget.PagerAdapter
import com.thiyagesh.admin.child.utlis.LoginActivity
import kotlinx.android.synthetic.main.activity_tour.*
import kotlinx.android.synthetic.main.image_item.view.*

class Tour : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tour)
        val loginButton = findViewById<Button>(R.id.login_Btn)
        val imagesList: ArrayList<Int> = ArrayList()
        imagesList.add(R.drawable.banner1)
        imagesList.add(R.drawable.banner2)
        imagesList.add(R.drawable.banner3)
        imagesList.add(R.drawable.banner4)
        imagesList.add(R.drawable.banner5)

        imageViewPager.adapter = ImagesAdapter(imagesList)
        dotsIndicator.setViewPager(imageViewPager)
        imageViewPager.adapter?.registerDataSetObserver(dotsIndicator.dataSetObserver)

        loginButton.setOnClickListener {
            val b = Intent (this, LoginActivity::class.java)
            startActivity(b)
        }
    }

    class ImagesAdapter(val media: ArrayList<Int>) : PagerAdapter() {

        override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object` as View

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val ctx = container.context
            val view = (ctx as Tour).layoutInflater.inflate(R.layout.image_item, container, false)
            view.imageView.setImageResource(media[position])
            container.addView(view)
            return view
        }

        override fun getCount(): Int = media.size

        override fun getItemPosition(`object`: Any): Int = super.getItemPosition(`object`)

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }
    }
}