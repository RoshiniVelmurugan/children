package com.thiyagesh.admin.child

import android.Manifest.permission
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import java.util.*
import kotlin.concurrent.schedule

class Splash_Activity : AppCompatActivity(){

    private val REQUEST_MULTIPLE_PERMISSIONS = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.splash_activity)

        Timer("SettingUp", false).schedule(5000) {
            move_replace()

        }
    }

    private fun checkAndRequestPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            val listOfPermissionsNedded: MutableList<String> =
                ArrayList()
            val camera: Int = ContextCompat.checkSelfPermission(
                Objects.requireNonNull(this),
                permission.CAMERA
            )
            val permissionPhoneState: Int =
                ContextCompat.checkSelfPermission(this, permission.READ_PHONE_STATE)
            val permissionCoarseLocation: Int =
                ContextCompat.checkSelfPermission(this, permission.ACCESS_COARSE_LOCATION)
            val recive: Int = ContextCompat.checkSelfPermission(this, permission.RECEIVE_SMS)
            val read: Int = ContextCompat.checkSelfPermission(this, permission.READ_SMS)
            val READ_EXTERNAL: Int =
                ContextCompat.checkSelfPermission(this, permission.READ_EXTERNAL_STORAGE)
            val ACCESS_FINE: Int =
                ContextCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION)
            if (camera != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.CAMERA)
            }
            if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.ACCESS_COARSE_LOCATION)
            }
            if (permissionPhoneState != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.READ_PHONE_STATE)
            }
            if (recive != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.RECEIVE_SMS)
            }
            if (read != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.READ_SMS)
            }
            if (READ_EXTERNAL != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.READ_EXTERNAL_STORAGE)
            }
            if (ACCESS_FINE != PackageManager.PERMISSION_GRANTED) {
                listOfPermissionsNedded.add(permission.ACCESS_FINE_LOCATION)
            }
            if (!listOfPermissionsNedded.isEmpty()) {
                requestPermissions(
                    listOfPermissionsNedded.toTypedArray(),REQUEST_MULTIPLE_PERMISSIONS
                )
            }
        }
        move_replace()
    }


    fun move_replace(){
        val b = Intent (this, Tour::class.java)
        startActivity(b)
    }



}