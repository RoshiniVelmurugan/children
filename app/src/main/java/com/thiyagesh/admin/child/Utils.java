/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thiyagesh.admin.child;


import android.content.Context;
import android.location.Location;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.model.LatLng;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.util.Date;

class Utils {

    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";
    static final String KEY_LOCATION_LAT = "KEY_LOCATION_LAT";
    static String KEY_LOCATION_LAT_LON = "KEY_LOCATION_LAT_LON";
    static final String KEY_LOCATION_LONG = "KEY_LOCATION_LONG";
    static final String KEY_LOCATION_LAT_NEW = "KEY_LOCATION_LAT_NEW";
    static final String KEY_LOCATION_LONG_NEW = "KEY_LOCATION_LONG_NEW";
    static final String KEY_DEVICE_ID = "KEY_DEVICE_ID";
    private static Object Location;

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link Location}.
     */
    static String getLocationText(Location location) {

        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    static String getLocationText(Context context) {

        KEY_LOCATION_LAT_LON = "(" + KEY_LOCATION_LAT + ", " + KEY_LOCATION_LONG + ")";

        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_LOCATION_LAT_LON,"00");

    }

    static void Setlocation(Context context,Location location){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(KEY_LOCATION_LAT, String.valueOf(location.getLatitude()))
                .apply();
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(KEY_LOCATION_LONG, String.valueOf(location.getLongitude()))
                .apply();


    }


    static void SetDeviceid(Context context,String devivceid){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(KEY_DEVICE_ID, devivceid)
                .apply();
    }

    static void Setlatlocation(Context context,String lat){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putLong(KEY_LOCATION_LAT_NEW, Long.parseLong(lat))
                .apply();
    }

    static void Setlonlocation(Context context,String lon){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putLong(KEY_LOCATION_LONG_NEW, Long.parseLong(lon))
                .apply();
    }

    static String getlatLocationText(Location location) {

        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    static String requesting_latitude(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_LOCATION_LAT,  "0.0");
    }
    static String requesting_longitude(Context context) {
        return String.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_LOCATION_LONG,  "0.0"));
    }
    static String requesting_latitude_long(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_LOCATION_LAT,  "0.0");
    }
    static String requesting_longitude_long(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_LOCATION_LONG, "0.0");
    }

    static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }

    static String getDeviceid(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_DEVICE_ID,  "fds");
    }

    @NotNull
    public static LatLng getLocationText() {
        return null;
    }
}
