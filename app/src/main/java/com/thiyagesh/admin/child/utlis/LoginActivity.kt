package com.thiyagesh.admin.child.utlis

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.content.Intent
import android.widget.TextView
import android.text.TextUtils
import android.util.Log

//import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.thiyagesh.admin.child.MapsActivity
import com.thiyagesh.admin.child.R


class LoginActivity : AppCompatActivity()  {

    //defining views
    private var buttonSignIn: Button? = null
    private var editTextEmail: EditText? = null
    private var editTextPassword: EditText? = null

    //firebase auth object
    private var firebaseAuth: FirebaseAuth? = null

    //progress dialog
    private var progressDialog: ProgressDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //getting firebase auth object
        firebaseAuth = FirebaseAuth.getInstance()
        val user = FirebaseAuth.getInstance().currentUser
        //if the objects getcurrentuser method is not null
        //means user is already logged in
        if (firebaseAuth!!.getCurrentUser() != null) {
            user?.let {
                for (profile in it.providerData) {
                    // UID specific to the provider
                    val uid = profile.uid
                    print(uid)
                } }
            //close this activity
            finish()
            //opening profile activity
            startActivity(Intent(applicationContext, MapsActivity::class.java))
        }

        //initializing views
        editTextEmail = findViewById<View>(R.id.editTextEmail) as EditText
        editTextPassword = findViewById<View>(R.id.editTextPassword) as EditText
        buttonSignIn = findViewById<View>(R.id.buttonSignin) as Button


        progressDialog = ProgressDialog(this)

        buttonSignIn!!.setOnClickListener {
            userLogin()
        }


    }

    //method for user login
    private fun userLogin() {
        val email = editTextEmail!!.text.toString().trim { it <= ' ' }
        val password = editTextPassword!!.text.toString().trim { it <= ' ' }


        //checking if email and passwords are empty
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_LONG).show()
            return
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show()
            return
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog!!.setMessage("Login Please Wait...")
        progressDialog!!.show()

        //logging in the user
        firebaseAuth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener( { task ->
                progressDialog!!.dismiss()
                //if the task is successfull
                if (task.isSuccessful) {
                    //start the profile activity
                    finish()
                    startActivity(Intent(applicationContext, MapsActivity::class.java))
                } else {
                    Toast.makeText(
                        this@LoginActivity,
                        "Login UnSucessful, Please try after some time.",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })

    }


    }
